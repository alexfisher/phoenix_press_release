<?php
/**
 * @file
 * phoenix_press_release.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function phoenix_press_release_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['views-press_release-block'] = array(
    'cache' => -1,
    'css_class' => 'contained',
    'custom' => 0,
    'delta' => 'press_release-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'page/press-releases',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'compro_adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'compro_adminimal',
        'weight' => 0,
      ),
      'compro_theme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'compro_theme',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  return $export;
}
